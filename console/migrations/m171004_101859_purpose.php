<?php

use yii\db\Migration;

class m171004_101859_purpose extends Migration
{
    public function safeUp()
    {
        $this->createTable('purpose', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'desc' => $this->text(),
            'date' => $this->date(),
            'status' => $this->integer(),
            'project_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%purpose}}');
    }
}
