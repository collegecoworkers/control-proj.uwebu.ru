<?php

use yii\db\Migration;

class m171004_102550_project extends Migration
{
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->text(),
            'date' => $this->date(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%project}}');
    }
}
