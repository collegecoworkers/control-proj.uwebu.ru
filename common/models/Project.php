<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Project extends ActiveRecord {

	public static function tableName() {
		return '{{%project}}';
	}

	public function attributeLabels() {
		return [
			'title' => 'Название',
			'desc' => 'Описание',
			'date' => 'Дата создания',
		];
	}

	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
