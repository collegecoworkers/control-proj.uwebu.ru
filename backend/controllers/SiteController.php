<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\Purpose;
use common\models\Project;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => ['class' => AccessControl::className(),
			'rules' => [
				[
					'actions' => [
						'signup',
						'login',
						'error'
					],
					'allow' => true,
				],
				[
					'actions' => [
						'logout',
						'index',
						'add-project',
						'update-project',
						'delete-project',
						'project',
						'down',
						'create',
						'update',
						'delete',
					],
					'allow' => true,
					'roles' => ['@'],
				],
			],
		],
	];
}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Project::find()->orderBy(['date' => SORT_ASC]);
		return $this->render('index', [
			'model' => $model
		]);
	}

	public function actionProject($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$project = Project::find()->where(['id' => $id])->one();
		$model = Purpose::find()->where(['project_id' => $id]);
		$done = Purpose::find()->where(['project_id' => $id, 'status' => 1])->count();
		$all = $model->count();
		return $this->render('project', [
			'project' => $project,
			'model' => $model,
			'done' => $done,
			'all' => $all,
		]);
	}

	public function actionCreate($id) {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Purpose();

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Purpose']['name'];
			$model->desc = Yii::$app->request->post()['Purpose']['desc'];
			$model->status = Yii::$app->request->post()['Purpose']['status'];
			$model->project_id = $id;
			$model->date = date('Y-m-d');
			$model->save();
			return $this->redirect(['/site/project', 'id' => $id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Purpose::allStatuses(),
			'projects' => $this->getProjects(),
		]);
	}

	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Purpose::findIdentity($id);

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Purpose']['name'];
			$model->desc = Yii::$app->request->post()['Purpose']['desc'];
			$model->status = Yii::$app->request->post()['Purpose']['status'];
			$model->save();
			return $this->redirect(['/site/project', 'id' => $model->project_id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Purpose::allStatuses(),
			'projects' => $this->getProjects(),
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Purpose::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionAddProject() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Project();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Project']['title'];
			$model->desc = Yii::$app->request->post()['Project']['desc'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-project', [
			'model' => $model,
		]);
	}

	public function actionUpdateProject($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Project::findOne($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Project']['title'];
			$model->desc = Yii::$app->request->post()['Project']['desc'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-project', [
			'model' => $model,
		]);
	}

	public function actionDeleteProject($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Project::findOne($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function getProjects() {
		$projects = [];
		$all_projects = Project::find()->all();

		for ($i=0; $i < count($all_projects); $i++) $projects[$all_projects[$i]->id] = $all_projects[$i]->title;

		return $projects;
	}

}
